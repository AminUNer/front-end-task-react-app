import * as ACTION_TYPES from "../../types/actionTypes";

// setting the initial state of our store
const INITIAL_STATE = {
  loading: false,
  content: {},
  videos: [],
  page: 1
};

const mainReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTION_TYPES.LOAD_DATA_PENDING:
      return {
        ...state,
        loading: true,
        page: action.payload
      };
    case ACTION_TYPES.LOAD_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        content: action.payload,
        videos: {
          ...state.videos,
          [state.page]: action.payload.data
        }
      };
    case ACTION_TYPES.LOAD_DATA_FAILURE:
      return { ...state, loading: false };
    default:
      return state;
  }
};
export default mainReducer;
