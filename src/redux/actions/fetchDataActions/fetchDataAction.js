import { API } from "../../../middleware/axiosDefaultConfig";
import {
  loadDataSucccess,
  loadDataPending,
  loadDataFailure
} from "../actionCreators";

/**
 * fetch data from api
 */
export const getDataFromApi = page => {
  return async dispatch => {
    try {
      // dispatch LOAD_DATA_PENDING action creator in order to display a loader while fetching data
      dispatch(loadDataPending(page));
      //using the new instance of axios that contains the default config to perform GET request
      let response = await API({
        url: `/channels/bestofthemonth/videos?page=${page}`,
        method: "GET"
      });
      if (response.status === 200) {
        // dispatch LOAD_DATA_SUCCESS with the recieved data in the payload
        dispatch(loadDataSucccess(response.data));
      }
    } catch (error) {
      // dispatch LOAD_DATA_FAILED
      dispatch(loadDataFailure());
      throw error;
    }
  };
};
