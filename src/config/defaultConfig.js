// base api url
let backendHost;

const hostname = window && window.location && window.location.hostname;

if(hostname === "localhost") {
    // develop backend link
    backendHost = "https://api.vimeo.com";
} else {
    // production backend link
    backendHost = "//PROD_BACKEND_HOST//";
}

export const BASE_URL = `${backendHost}`;
