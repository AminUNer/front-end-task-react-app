import React, { useState } from "react";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import CommentIcon from "@material-ui/icons/Comment";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import VideoLibrary from "@material-ui/icons/VideoLibrary";
import Language from "@material-ui/icons/Language";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import moment from "moment";
import Vimeo from '@u-wave/react-vimeo';
import "../assets/styles/VideoCard.module.css";

// prepare styles for the @material-ui components
const useStyles = makeStyles(theme => ({
  container: {
    display: "grid",
    gridTemplateColumns: "repeat(12, 1fr)",
    gridGap: theme.spacing(0),
    alignItems: "center",
    justifyContent: "space-around"
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  card: {},
  media: {
    height: 0,
    paddingTop: "56.25%"
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  }
}));
// functional component VideoCard with pagination props video
const VideoCard = ({ video }) => {
  // assign the prepeared style above to classes constant
  const classes = useStyles();
  // declare boolean localState expandedVideoDetails intialized to false
  const [expandedVideoDetails, setExpandedVideoDetails] = useState(false);
  // switch the expandedVideoDetails boolean value
  const handleExpandClick = () => {
    setExpandedVideoDetails(!expandedVideoDetails);
  };

  return (
    <Grid className="mainContent" container>
      <Grid item xs={12} lg={3} className="imageContent">
        <img src={video.pictures.sizes[2].link} alt="thumbnail" />
      </Grid>
      <Grid item xs={12} lg={9}>
        <Card className={classes.card}>
          <CardHeader
            className="cardHeaderContent"
            avatar={
              <Avatar aria-label="user-avatar" className={classes.avatar}>
                {video.user.name[0]}
              </Avatar>
            }
            title={video.user.name}
            subheader={moment(video.created_time).format("LL")}
          />
          <CardContent>
            <Typography gutterBottom component="h2">
              {video.name}
            </Typography>
            <Typography
              color="textSecondary"
              component="p"
              className={!expandedVideoDetails ? "paragraphContent" : ""}
            >
              {video.description}
            </Typography>
          </CardContent>
          <CardActions disableSpacing>
            <IconButton aria-label="like">
              {video.metadata.connections.likes.total}
              <FavoriteIcon />
            </IconButton>
            <IconButton aria-label="comment">
              {video.metadata.connections.comments.total}
              <CommentIcon />
            </IconButton>
            <IconButton aria-label="duration">
              {Math.floor(video.duration / 60) + ":"}
              {(video.duration % 60).toString().length === 1
                ? "0" + (video.duration % 60)
                : video.duration % 60}
              <VideoLibrary />
            </IconButton>
            {video.language ? (
              <IconButton aria-label="language">
                {video.language}
                <Language />
              </IconButton>
            ) : null}
            <IconButton
              className={clsx(classes.expand, {
                [classes.expandOpen]: expandedVideoDetails
              })}
              onClick={() => handleExpandClick(video.resource_key)}
              aria-expanded={expandedVideoDetails}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </CardActions>
          <Collapse in={expandedVideoDetails} timeout="auto" unmountOnExit>
            <CardContent>
            <Vimeo
                video={video.uri.substr(8, 17)}
                autoplay
                responsive
            />
            </CardContent>
          </Collapse>
        </Card>
      </Grid>
    </Grid>
  );
};

export default VideoCard;
