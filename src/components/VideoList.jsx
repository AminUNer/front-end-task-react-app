import React, { Fragment, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getDataFromApi } from "../redux/actions/fetchDataActions/fetchDataAction";
import Pagination from "react-js-pagination";
import VideoCard from "./VideoCard";
import "../assets/styles/VideoList.module.css";

// functional component VideoList with pagination props
const VideoList = ({ pagination }) => {
  // declare useDispatch and useSelector hooks in order to read state from the store or dispatch a new action
  const selector = useSelector(state => state);
  const dispatch = useDispatch();
  // declare localState pageNumber to inisialise pageNumber to 1
  const [pageNumber, setPageNumber] = useState(1);
  // assign the video list loaded in the store to the global variable videos
  const videos = Object.assign([], selector.videos[pageNumber]);

  const [isFetchedArray, setIsFetchedArray] = useState([1]);

  const [isAlreadyFetched, setIsAlreadyFetched] = useState(false);

  // handle onChange evenet in the pagination component
  const onPageNumberChange = page => {
    // setting the pageNumber to the current value
    setPageNumber(page);
    if (!isFetchedArray.includes(page)) {
      // fetching data according to the new pageNumber
      dispatch(getDataFromApi(page));
      // add the loaded page to the fetchedDataArray
      setIsFetchedArray([...isFetchedArray, page]);
      // change the isAlreadyFetched boolean value to false
      setIsAlreadyFetched(false);
    } else {
      // change the isAlreadyFetched boolean value to true in order to change the activePage pagination paramater
      setIsAlreadyFetched(true);
    }
  };

  return (
    <Fragment>
      <div className="paginationContent">
        <Pagination
          activePage={isAlreadyFetched ? pageNumber : pagination.page}
          itemsCountPerPage={pagination.per_page}
          totalItemsCount={pagination.total}
          pageRangeDisplayed={5}
          onChange={onPageNumberChange}
        />
      </div>
      {selector.loading ? (
        <div className="loadingImage">
          <img
            src={require("../assets/images/vimeo-loading.gif")}
            alt="loading..."
          />
        </div>
      ) : (
        videos.map(video => (
          <VideoCard video={video} key={video.resource_key} />
        ))
      )}
    </Fragment>
  );
};
export default VideoList;
