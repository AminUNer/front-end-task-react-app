import axios from "axios";
import { BASE_URL } from "../config/defaultConfig";

// Set config defaults when creating the API
export const API = axios.create({
  baseURL: BASE_URL,
  ResponseType: "json",
  headers: { Authorization: "bearer 57930b58a1a128c6c01489efd1ab3163" }
});
