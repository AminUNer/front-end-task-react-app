import React, { useEffect } from "react";
import VideoList from "./components/VideoList";
import { useSelector, useDispatch } from "react-redux";
import { getDataFromApi } from "./redux/actions/fetchDataActions/fetchDataAction";
import "./assets/styles/App.css";

/**
 * functional component App
 */
const App = () => {
  // declare useDispatch and useSelector hooks in order to read state from the store or dispatch a new action
  const dispatch = useDispatch();
  const selector = useSelector(state => state);
  // initialize pagination parameters
  const pagination = {
    page: selector.content.page,
    total: selector.content.total,
    per_page: selector.content.per_page
  };
  /** equivalent to componentDidMount with second params as [] */
  useEffect(() => {
    // fetch data from API of the first page
    dispatch(getDataFromApi(1));
  }, []);

  return (
    // call the VideoList component and add props pagination
    <VideoList pagination={pagination} />
  );
};
export default App;
